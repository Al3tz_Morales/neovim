set encoding=utf8

" Python 3 interpreter
let g:python3_host_prog='/usr/bin/python3'

" Clipboard
set clipboard+=unnamedplus

" Map leaderkey
let mapleader = ","

" Ability to add python breakpoints
" (I use ipdb, but you can change it to whatever tool you use for debugging)
" au FileType python map <silent> <leader>b Ofrom pudb.remote import " 
" set_trace;set_trace(term_size=(160, 40), host='0.0.0.0', port=6900)<esc>

set termguicolors  " Activa true colors en la terminal
set background=dark  " Fondo del tema: dark/light
colorscheme NeoSolarized  " Activa tema NeoSolarized

"Yggdroot/indentLine -----------------------
" No mostrar en ciertos tipos de buffers y archivos
let g:indentLine_fileTypeExclude = ['text', 'sh', 'help', 'terminal']
let g:indentLine_bufNameExclude = ['NERD_tree.*', 'term:.*']


" Invertir direccion de navegacion (de arriba a abajo)
" let g:SuperTabDefaultCompletionType = '<c-n>'


" Nombre del archivo generado
let g:gutentags_ctags_tagfile = '.tags'


" Maps requeridos
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
" Quitar resaltado luego de buscar
let g:incsearch#auto_nohlsearch = 1

" Actualizar barra cada 250 mili segundos
set updatetime=250


"" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>

"" Tabs
nnoremap <Tab> gt
nnoremap <S-Tab> gT
nnoremap <silent> <S-t> :tabnew<CR>

nnoremap <silent> <leader>b :Buffers<CR>

"" Copy/Paste/Cut
if has('unnamedplus')
  set clipboard+=unnamed,unnamedplus
endif

" Fix paste from mouse
" set clipboard+=unnamedplus

noremap YY "+y<CR>
noremap <leader>p "+gP<CR>
noremap XX "+x<CR>

if has('macunix')
  " pbcopy for OSX copy/paste
  vmap <C-x> :!pbcopy<CR>
  vmap <C-c> :w !pbcopy<CR><CR>
endif

"" Buffer nav
noremap <leader>z :bp<CR>
noremap <leader>q :bp<CR>
noremap <leader>x :bn<CR>
noremap <leader>w :bn<CR>

"" Close buffer
noremap <leader>c :bd<CR>

"" Clean search (highlight)
nnoremap <silent> <leader><space> :noh<cr>

"" Switching windows
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h

" python
" vim-python
augroup vimrc-python
  autocmd!
  autocmd FileType python setlocal expandtab shiftwidth=4 tabstop=8 colorcolumn=79
      \ formatoptions+=croq softtabstop=4
      \ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

"*****************************************************************************
"" Convenience variables
"*****************************************************************************


" With this tip, just selecting text with mouse in gvim, will copy that text to the clipboard 
" for pasting into other applications (on Windows and on Xwindows, ala Xterms). 
" http://vim.wikia.com/wiki/Auto_copy_the_mouse_selection
" Method 1 (for Xwindows and mswindows), this map is different from the one shown in vim documentation
:noremap <LeftRelease> "+y<LeftRelease>

" loading the plugin 
let g:webdevicons_enable = 1

" adding the flags to NERDTree 
let g:webdevicons_enable_nerdtree = 1

" adding to vim-airline's tabline 
let g:webdevicons_enable_airline_tabline = 1

" adding to vim-airline's statusline 
let g:webdevicons_enable_airline_statusline = 1

" adding to flagship's statusline 
let g:webdevicons_enable_flagship_statusline = 1

" turn on/off file node glyph decorations (not particularly useful)
" let g:WebDevIconsUnicodeDecorateFileNodes = 1

" use double-width(1) or single-width(0) glyphs 
" only manipulates padding, has no effect on terminal or set(guifont) font
let g:WebDevIconsUnicodeGlyphDoubleWidth = 1

" whether or not to show the nerdtree brackets around flags 
let g:webdevicons_conceal_nerdtree_brackets = 0

" the amount of space to use after the glyph character (default ' ')
let g:WebDevIconsNerdTreeAfterGlyphPadding = '  '

" Force extra padding in NERDTree so that the filetype icons line up vertically 
let g:WebDevIconsNerdTreeGitPluginForceVAlign = 1

let g:WebDevIconsUnicodeDecorateFileNodesDefaultSymbol = 'ƛ'
let g:WebDevIconsUnicodeDecorateFolderNodes = 1

" after a re-source, fix syntax matching issues (concealing brackets):
if exists('g:loaded_webdevicons')
    call webdevicons#refresh()
endif

autocmd FileType sh,python   let b:comment_leader = '# '
autocmd FileType vim              let b:comment_leader = '" '
noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>

map <leader>fi :setlocal foldmethod=indent<cr>
map <leader>fs :setlocal foldmethod=syntax<cr>
map <C-F12> :!ctags -R --exclude=.git --exclude=logs --exclude=doc .<CR>

" Reload files
set autoread
au FocusGained * :checktime

" Tabs click
set mouse=a

" Mappings from https://stsewd.dev/es/posts/neovim-installation-configuration/
nnoremap <leader>s :w<CR>  " Guardar con <líder> + s

" Usar <líder> + y para copiar al portapapeles
vnoremap <leader>y "+y
nnoremap <leader>y "+y

" Usar <líder> + d para cortar al portapapeles
vnoremap <leader>d "+d
nnoremap <leader>d "+d

" Usar <líder> + p para pegar desde el portapapeles
nnoremap <leader>p "+p
vnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>P "+P

" Moverse al buffer siguiente con <líder> + l
nnoremap <leader>l :bnext<CR>

" Moverse al buffer anterior con <líder> + j
nnoremap <leader>j :bprevious<CR>

" Cerrar el buffer actual con <líder> + q
nnoremap <leader>q :bdelete<CR>

" From https://www.cyberciti.biz/faq/how-to-reload-vimrc-file-without-restarting-vim-on-linux-unix/
" Edit vimr configuration file
nnoremap <Leader>ve :e $MYVIMRC<CR>
" " Reload vimr configuration file
nnoremap <Leader>vr :source $MYVIMRC<CR>

" Autopep8
autocmd FileType python noremap <buffer> <F8> :call Autopep8()<CR>
let g:autopep8_pep8_passes=100
let g:autopep8_max_line_length=79
let g:autopep8_diff_type='vertical'
let g:autopep8_diff_type='vertical'

" using Source Code Pro
" set guifont=Source\ Code\ Pro\ for\ Powerline:h12
set guifont=FiraCode_Nerd_Font:h12


" From https://vim.fandom.com/wiki/Easier_buffer_switching
" Buffers - explore/next/previous: Alt-F12, F12, Shift-F12.
" nnoremap <silent> <M-F12> :BufExplorer<CR>
nnoremap <silent> <F12> :bn<CR>
nnoremap <silent> <S-F12> :bp<CR>

nnoremap <silent> <F11> :BufExplorer<CR>
nnoremap <silent> <s-F11> :ToggleBufExplorer<CR>
nnoremap <silent> <m-F11> :BufExplorerHorizontalSplit<CR>
nnoremap <silent> <c-F11> :BufExplorerVerticalSplit<CR>

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
" let g:UltiSnipsEditSplit="vertical"

" map <c-f> :call JsBeautify()<cr>
" fisadev/vim-isort
" let g:vim_isort_map = '<C-i>'

set statusline+=%{zoom#statusline()}

" Select and copy all lines
" https://superuser.com/questions/227385/how-do-i-select-all-text-in-vi-vim/1230483
map <C-a> <esc>ggVG<CR>


let g:AutoClosePreserveDotReg = 0

" Resize
" nnoremap <silent> <Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
" nnoremap <silent> <Leader>- :exe "resize " . (winheight(0) * 2/3)<CR>

" Pydocstring
let g:pydocstring_doq_path = '/home/alex/.local/bin/doq'

" Coc-prettier
command! -nargs=0 Prettier :CocCommand prettier.formatFile
vmap <leader>fo  <Plug>(coc-format-selected)
nmap <leader>fo  <Plug>(coc-format-selected)

" Fold autosave
augroup AutoSaveFolds
  autocmd!
  autocmd BufWinLeave * mkview
  autocmd BufWinEnter * silent loadview
augroup END


source ~/.config/nvim/addons/nerdtree.vim
source ~/.config/nvim/addons/autoflake.vim
source ~/.config/nvim/addons/airline.vim
source ~/.config/nvim/addons/utils.vim
source ~/.config/nvim/addons/coc-setting.vim
