if exists('g:GtkGuiLoaded')
  "GuiFont Ubuntu Mono derivative PowerLine:h12
  call rpcnotify(1, 'Gui', 'Option', 'Tabline', 0)
  call rpcnotify(1, 'Gui', 'Option', 'Cmdline', 1)
  " call rpcnotify(1, 'Gui', 'Font', 'FiraCode Nerd Font 12')
  " call rpcnotify(1, 'Gui', 'FontFeatures', 'PURS, cv17')
  let g:GuiInternalClipboard = 1
endif
